import axios from "axios";

import {
    FETCH_KELAS,
    FETCH_KELAS_FAILED,
    FETCH_KELAS_SUCCESS,
} from './actionsTypes';

import { fetchListKelasApi } from '../../api';

export const fetchListKelas = () => {
    return (dispatch) => {
        dispatch({
            type: FETCH_KELAS
        });
        axios
            .get(fetchListKelasApi)
            .then((response) => {
                dispatch({
                    listKelas: response.data,
                    type: FETCH_KELAS_SUCCESS
                });
            })
            .catch((error) => {
                dispatch({
                    error: error,
                    type: FETCH_KELAS_FAILED
                });
            });
    }
}