import axios from "axios";
import { SENDING_REQUEST, SET_AUTH, SET_USER, SET_USER_FAILURE, SET_USER_SUCCESS } from "./actionsTypes";
import { fetchProfileApi } from '../../api';
import auth from "../../auth";
import { push } from "connected-react-router";


export const login = (user) => {
    return (dispatch) => {
        dispatch(sendingRequest(true));
        auth.login(user)

        dispatch(setUser());
        dispatch(setAuthState(true));
        dispatch(sendingRequest(false));

        dispatch(push("/dashboard/user"));
    }
}

export function logout() {
    return (dispatch) => {
        dispatch(sendingRequest(true));
        dispatch(setAuthState(false));
        dispatch(setUser({}));
        auth.logout();
        dispatch(sendingRequest(false));
    }
}

export const sendingRequest = (sending) => {
    return {
        type: SENDING_REQUEST,
        sending
    }
}

export const setUser = () => {
    return (dispatch) => {
        dispatch({
            type: SET_USER,
        });
        axios
            .get(fetchProfileApi)
            .then((response) => {
                dispatch({
                    user: response.data,
                    type: SET_USER_SUCCESS
                })
            })
            .catch((error) => {
                dispatch({
                    payload: error,
                    type: SET_USER_FAILURE
                });
            })
    }
}

export const setAuthState = (newAuthState) => {
    return {
        type: SET_AUTH,
        newAuthState
    }
}