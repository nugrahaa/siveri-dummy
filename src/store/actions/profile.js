import axios from "axios";

import {
    FETCH_PROFILE,
    FETCH_PROFILE_SUCCESS,
    FETCH_PROFILE_FAILED
} from './actionsTypes'

import { fetchProfileApi } from '../../api';

export const fetchProfile = () => {
    return (dispatch) => {
        dispatch({
            type: FETCH_PROFILE
        });
        axios
            .get(fetchProfileApi)
            .then((response) => {
                dispatch({
                    user: response.data,
                    type: FETCH_PROFILE_SUCCESS,
                });
            })
            .catch((error) => {
                dispatch({
                    error: error,
                    type: FETCH_PROFILE_FAILED
                })
            })
    }
}
