import { FETCH_KELAS, FETCH_KELAS_FAILED, FETCH_KELAS_SUCCESS } from '../actions/actionsTypes';
import { updateObject } from '../utility';

const initialState = {
    error: null,
    isLoading: false,
    listKelas: {}
}

const listKelasReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_KELAS:
            return updateObject(state, {isLoading: true})
        case FETCH_KELAS_SUCCESS:
            return updateObject(state, {isLoading: false, listKelas: action.listKelas})
        case FETCH_KELAS_FAILED:
            return updateObject(state, {isLoading: false, error: action.error})
        default:
            return state
    }
}

export default listKelasReducer;