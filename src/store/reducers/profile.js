import { FETCH_PROFILE, FETCH_PROFILE_FAILED, FETCH_PROFILE_SUCCESS } from '../actions/actionsTypes';
import { updateObject } from '../utility';

const initialState = {
    error: null,
    isLoading: false,
    user: {}
};

const dashBoardUserReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PROFILE:
            return updateObject(state, {isLoading: false})
        case FETCH_PROFILE_SUCCESS:
            return updateObject(state, {isLoading: true, user: action.user})
        case FETCH_PROFILE_FAILED:
            return updateObject(state, {error: action.error})
        default:
            return state
    }
} 

export default dashBoardUserReducer;