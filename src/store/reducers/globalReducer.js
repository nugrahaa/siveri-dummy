import { SENDING_REQUEST, SET_USER, SET_USER_FAILURE, SET_USER_SUCCESS, SET_AUTH } from '../actions/actionsTypes';
import { fromJS } from "immutable";
import { isEmpty } from 'lodash';
import auth from "../../auth";

const isLoggedIn = auth.loggedIn();

const initialState = fromJS({
    currentlySending: false,
    isLoading: false,
    loggedIn: !isEmpty(isLoggedIn),
    user: {},
    error: null,
    hello: "Hello World"
});

const globalReducer = (state = initialState, action) => {
    switch (action.type) {
        case SENDING_REQUEST:
            return state.set("currentlySending", action.sending)           
        case SET_USER:
            return state.set("isLoading", true)
        case SET_USER_SUCCESS:
            return state.set("user", action.user).set("isLoading", false);
        case SET_USER_FAILURE:
            return state.set("error", action.payload);
        case SET_AUTH:
            return state.set("loggedIn", action.newAuthState)
        default:
            return state
    }
} 

export default globalReducer;