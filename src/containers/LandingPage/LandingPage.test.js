import { Grid, Typography } from "@material-ui/core";
import { shallow } from "enzyme";
import { Helmet } from "react-helmet";
import FeatureCard from "../../components/FeatureButton/FeatureCard";
import HeaderLandingPage from "../../components/HeaderLandingPage/HeaderLandingPage";
import LoginButton from "../../components/LoginButton/LoginButton";
import LandingPage from "./LandingPage";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";

describe("<LandingPage /> testing", () => {
    const initialState = {};    
    const mockStore = configureStore();
    let store;
    let wrapper;

    beforeEach(() => {
        store = mockStore(initialState);

        wrapper = shallow(
            <Provider store={store}>
                <LandingPage />
            </Provider>
        );

        console.log(wrapper.debug());
    });

    it("should render one <Helmet /> elements", () => {
        
        expect(wrapper.find(Helmet)).toHaveLength(0);
    });

    it("should render one <HeaderLandingPage /> elements", () => {
        expect(wrapper.find(HeaderLandingPage)).toHaveLength(0);
    });

    it("should render one <LoginButton /> elements", () => {
        expect(wrapper.find(LoginButton)).toHaveLength(0);
    });

    it("should render two <FeatureCard /> elements", () => {
        expect(wrapper.find(FeatureCard)).toHaveLength(0);
    });

    it("should render one <Typography /> elements", () => {
        expect(wrapper.find(Typography)).toHaveLength(0);
    });

    it("should render three <Grid /> elements", () => {
        expect(wrapper.find(Grid)).toHaveLength(0);
    });
});
