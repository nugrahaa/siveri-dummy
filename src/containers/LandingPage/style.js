import styled from "styled-components";

const LandingPageStyled = styled.div`
    margin: auto;
    padding-right: 50px;
    vertical-align: middle;
    position: absolute;
    top: 8%;

    .rektorat {
        width: 200px;
        margin-bottom: 20px;
    }
    .info {
        text-align: center;
        font-size: 12px;
        font-style: italic;
    }
    .feature {
        margin-bottom: 20px;
    }
    .login-button {
        margin-top: 30px;
    }
    .feature-container {
        background-color: #d5cec5;
        border-radius: 20px;
        padding: 60px 0;
    }
`;

export default LandingPageStyled;
