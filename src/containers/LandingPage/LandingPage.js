import { Grid, Typography } from "@material-ui/core";
import React, { Component } from "react";
import { Helmet } from "react-helmet";
import HeaderLandingPage from "../../components/HeaderLandingPage/HeaderLandingPage";
import favicon from "./assets/ui.png";
import LandingPageStyled from "./style";
import FeatureCard from "../../components/FeatureButton/FeatureCard";
import LoginButton from "../../components/LoginButton/LoginButton";
import rektorat from "./assets/rektorat.png";
import { connect } from "react-redux";
import { login } from '../../store/actions/globalActions';
import { loggingInApi } from "../../api";
import { push } from "connected-react-router";

class LandingPage extends Component {

    handleLogin = () => {
        const loginWindow = window.open(
            loggingInApi,
            "_blank",
            "width=800,height=800",
            "toolbar=0,location=0,menubar=0"
        );

        const getUserDataInterval = setInterval(() => {
            if (loginWindow.closed) {
                clearInterval(getUserDataInterval);
            }
            loginWindow.postMessage("Made by PPL Funtech 2021", loggingInApi);
        }, 1000)
    };

    receiveLoginData = (event) => {
        const origin = event.origin || event.originalEvent.origin
        const user = event.data

        if (loggingInApi.startsWith(origin)) {
            this.props.login(user)
        }
    }

    componentDidMount() {
        window.addEventListener("message", this.receiveLoginData, false)
    }

    render() {
        return (
            <LandingPageStyled>
                <Helmet>
                    <title>FEB UI</title>
                    <link rel="icon" type="image/png" href={favicon} alt="favicon"/>
                </Helmet>
                <Grid
                    container
                    justify="center"
                    alignItems="center"
                    direction="row"
                >
                    <Grid
                        container
                        direction="column"
                        justify="center"
                        alignItems="center"
                        item
                        md={7}
                    >
                        <Grid item md={12}>
                            <HeaderLandingPage />
                        </Grid>
                        <Grid item md={12}>
                            <img src={rektorat} className="rektorat" alt="Rektorat UI"/>
                        </Grid>
                        <Grid item md={10}>
                            <Typography className="info">
                                Silahkan pilih layanan yang ingin dipilih dan
                                melakukan sign in untuk dapat menggunakan
                                layanan tersebut. Apabila menemukan kesulitan,
                                silahkan menghubungi birpend di 08111927296 atau
                                birpend.febui@ui.ac.id
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid
                        container
                        justify="center"
                        alignItems="center"
                        item
                        md={5}
                        direction="column"
                        className="feature-container"
                    >
                        <Grid item className="feature">
                            <FeatureCard
                                type="MPKT 5 SKS"
                                detail="Layanan ini diperuntukan bagi mahasiswa yang mengambil dan dosen pengampu MPKT 5 SKS terkait dengan pengumpulan tugas"
                            />
                        </Grid>
                        <Grid item className="feature">
                            <FeatureCard
                                type="Magang"
                                detail="Pelaporan magang mahasiswa agar dapat dinilai dan disetujui oleh dosen sebagai bahan pertimbangan kelulusan"
                            />
                        </Grid>
                        <Grid item className="login-button">
                            <LoginButton clicked={() => this.handleLogin()}>
                                LOGIN DENGAN SSO
                            </LoginButton>
                        </Grid>
                    </Grid>
                </Grid>
            </LandingPageStyled>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        push: (url) => dispatch(push(url)),
        login: (user) => dispatch(login(user))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);
