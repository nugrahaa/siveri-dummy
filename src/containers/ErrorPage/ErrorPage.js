import { Grid, Typography } from "@material-ui/core";
import ErrorImage from "../../assets/no_data.png";
import React, { Component } from "react";

export default class ErrorPage extends Component {
    render() {
        return (
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
            >
                <Grid item>
                    <img
                        src={ErrorImage}
                        alt="No Data"
                        style={{ height: "200px" }}
                    />
                </Grid>
                <Grid item>
                    <Typography variant="h5">
                        Sepertinya akun kamu tidak valid
                    </Typography>
                </Grid>
            </Grid>
        );
    }
}
