import React from 'react';
import { shallow } from 'enzyme';
import { Grid, Typography } from '@material-ui/core';
import ErrorPage from './ErrorPage';

describe('<ErrorPage /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ErrorPage />);
  });

  it('should render three <Grid />', () => {
    expect(wrapper.find(Grid)).toHaveLength(3);
  });

  it('should render one <Typography />', () => {
    expect(wrapper.find(Typography)).toHaveLength(1);
  });
});
