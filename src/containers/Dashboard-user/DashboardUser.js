import { Grid, Typography } from "@material-ui/core";
import React, { Component } from "react";
import { Helmet } from "react-helmet";
import favicon from "./assets/ui.png";
import DashboardStyled from "./style";
import ProfileCard from "../../components/ProfileCard/ProfileCard";
import ServiceCard from "../../components/ServiceCard/ServiceCard";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { fetchProfile } from "../../store/actions/profile";
import { loggingOutApi } from "../../api";
import { logout } from '../../store/actions/globalActions';
import NavbarUser from "../../components/NavbarUser/NavbarUser";

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        this.props.fetchProfile();
    }

    handleLogout = () => {
        const logoutWindow = window.open(loggingOutApi, "_self");
        const getUserDataInterval = setInterval(() => {
            console.log("INTERVAL")
            if (logoutWindow.closed) {
                clearInterval(getUserDataInterval);
            }

            logoutWindow.postMessage("Made By Fentech PPL 2021", loggingOutApi);
        }, 1000);
        logout();
        localStorage.removeItem("mpkt2021")
    };

    render() {
        if (this.props.error) {
            this.props.push("/error");
        }

        if (this.props.isLoading) {
            const data = this.props.user[0];
            return (
                <DashboardStyled>
                    <Helmet>
                        <title>FEB UI</title>
                        <link rel="icon" type="image/png" href={favicon} />
                    </Helmet>
                    <NavbarUser clicked={() => this.handleLogout()} 
                        name={data.full_name}
                        role={data.profile.role}
                        npm={data.profile.npm}/>
                    <Grid container direction="row">
                        <Grid item md={12} className="dashboard-container">
                            <Typography variant="h3" className="info">
                                Dashboard
                            </Typography>
                        </Grid>
                        <Grid item md={12} className="liner-container"></Grid>
                        <Grid
                            container
                            direction="row"
                            justify="space-around"
                            item
                            md={12}
                        >
                            <Grid item md={4} className="profile-component">
                                <ProfileCard
                                    name={data.full_name}
                                    role={data.profile.role}
                                    npm={data.profile.npm}
                                    major={data.profile.study_program}
                                    faculty={data.profile.faculty}
                                ></ProfileCard>
                            </Grid>
                            <Grid
                                container
                                item
                                md={6}
                                className="service-container"
                            >
                                <Grid item>
                                    <ServiceCard type="MPKT 5 SKS" />
                                </Grid>

                                <Grid item>
                                    <ServiceCard type="Magang" />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </DashboardStyled>
            );
        }
        return (
            <DashboardStyled>
                <Helmet>
                    <title>FEB UI</title>
                    <link rel="icon" type="image/png" href={favicon} />
                </Helmet>
            </DashboardStyled>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.dashBoardUserReducer.user,
        isLoading: state.dashBoardUserReducer.isLoading,
        isError: state.dashBoardUserReducer.error
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        push: (url) => dispatch(push(url)),
        fetchProfile: () => dispatch(fetchProfile()),
        logout: () => dispatch(logout())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
