import styled from "styled-components";

const DashboardStyled = styled.div`
    margin: auto;
    position: absolute;
    .headline-container {
        border-radius: 0px;
        box-shadow: 0px 5px 1px rgba(0, 0, 0, 0.2);
    }

    .dashboard-container{
        border-radius: 0px;
        box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.2);
        padding: 10px 0;
        margin-top : 2%;
    }

    .info{
        padding-left:5%;
        margin-bottom:5px;
    }
    
    .service-container{
        height:0px;
        border-radius: 20px;
        padding: 20px 0;
        outline-color: black;
        margin-top:6%;
    }

    .profile-component {
        margin-bottom: 50px;
    }
`;

export default DashboardStyled;
