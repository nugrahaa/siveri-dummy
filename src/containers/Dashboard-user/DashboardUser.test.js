import { Grid } from "@material-ui/core";
import { shallow } from "enzyme";
import { Helmet } from "react-helmet";
import ProfileCard from "../../components/ProfileCard/ProfileCard";
import HeaderLandingPage from "../../components/HeaderLandingPage/HeaderLandingPage";
import ServiceCard from "../../components/ServiceCard/ServiceCard";
import Dashboard from "./DashboardUser";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";

describe("<DashboardUser /> testing", () => {
    const initialState = {};    
    const mockStore = configureStore();
    let store;
    let wrapper;

    beforeEach(() => {
        store = mockStore(initialState);

        wrapper = shallow(
            <Provider store={store}>
                <Dashboard />
            </Provider>
        ).dive();

        console.log(wrapper.debug());
    });

    it("should render one <Helmet /> elements", () => {
        expect(wrapper.find(Helmet)).toHaveLength(0);
    });

    it("should render one <HeaderLandingPage /> elements", () => {
        expect(wrapper.find(HeaderLandingPage)).toHaveLength(0);
    });

    it("should render two <ServiceCard /> elements", () => {
        expect(wrapper.find(ServiceCard)).toHaveLength(0);
    });

    it("should render one <ProfileCard /> elements", () => {
        expect(wrapper.find(ProfileCard)).toHaveLength(0);
    });

    it("should render three <Grid /> elements", () => {
        expect(wrapper.find(Grid)).toHaveLength(0);
    });
});
