import LandingPage from "../LandingPage/LandingPage";
import DashboardUser from "../Dashboard-user/DashboardUser";
import Dashboard from "../Dashboard/Dashboard"
import ListKelas from "../ListKelas/ListKelas";
import CreateKelas from "../CreateKelas/CreateKelas"
import DashboardKelas from "../DashboardKelas/DashboardKelas";
import ErrorPage from "../ErrorPage/ErrorPage";

export const routes = [
    {
        component: LandingPage,
        exact: true,
        path: '/'
    },
    {
        component: DashboardUser,
        exact: true,
        path: '/dashboard/user'
    },
    {
        component: Dashboard,
        exact: true,
        path: '/dashboard'
    },
    {
        component: ListKelas,
        exact: true,
        path: '/kelas'
    },
    {
        component: CreateKelas,
        exact: true,
        path: '/kelas/create'
    },
    {
        component: DashboardKelas,
        exact: true,
        path: '/kelas/dashboard'
    },
    {
        component: ErrorPage,
        exact: true,
        path: '/error'
    }
];
