import { shallow } from 'enzyme';
import { Redirect, Route, Switch } from 'react-router';
import App from './App';

describe("Hello World Testing", () => {

  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<App />);
  });

  it('should render one <Switch /> elements', () => {
    expect(wrapper.find(Switch)).toHaveLength(1);
  });

  it('should render <Router /> elements', () => {
    expect(wrapper.find(Route)).toBeTruthy();
  });

  it('should render <Redirect /> elements', () => {
    expect(wrapper.find(Redirect)).toHaveLength(1);
  });

});