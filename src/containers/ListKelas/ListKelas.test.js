import React from 'react';
import { shallow } from 'enzyme';
import ListKelas from './ListKelas';
import ListKelasStyled from './style';
import NavbarUser from '../../components/NavbarUser/NavbarUser';
import { Divider, Grid, Typography } from '@material-ui/core';

describe('<ListKelas /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<ListKelas />);
  });

  it('should render one <ListKelasStyled />', () => {
    expect(wrapper.find(ListKelasStyled)).toHaveLength(1);
  });

  it('should render one <NavbarUser />', () => {
    expect(wrapper.find(NavbarUser)).toHaveLength(1);
  });

  it('should render three <Grid />', () => {
    expect(wrapper.find(Grid)).toHaveLength(3);
  });

  it('should render one <Typography />', () => {
    expect(wrapper.find(Typography)).toHaveLength(1);
  });

  it('should render one <Divider />', () => {
    expect(wrapper.find(Divider)).toHaveLength(1);
  });

});
