import { Divider, Grid, Typography } from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import CardKelas from "../../components/CardKelas/CardKelas";
import NavbarUser from "../../components/NavbarUser/NavbarUser";
import { fetchListKelas } from "../../store/actions/listKelas";
import { fetchProfile } from "../../store/actions/profile";
import ListKelasStyled from "./style";

class ListKelas extends Component {

    componentDidMount() {
        this.props.fetchProfile();
        this.props.fetchListKelas();
    }

    render() {
        const listKelas = this.props.listKelas;

        return (
            <ListKelasStyled>
                <NavbarUser />
                <Grid className="main-container">
                    <Grid className="header-container">
                        <Typography className="title" variant="h4">
                            Daftar Kelas
                        </Typography>
                        <Divider />
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        justify="space-around"
                        alignItems="center"
                    >
                        <CardKelas />
                        <CardKelas />
                        <CardKelas />
                    </Grid>
                </Grid>
            </ListKelasStyled>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        listKelas: state.listKelasReducer.listKelas
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchProfile: () => dispatch(fetchProfile()),
        fetchListKelas: () => dispatch(fetchListKelas())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListKelas);
