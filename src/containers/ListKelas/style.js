import styled from "styled-components";

const ListKelasStyled = styled.div`
    .title {
        margin-bottom: 10px;
    }
    .main-container {
        margin: 30px 50px;
    }
    .card-container {
        margin-top: 30px;
    }
`;

export default ListKelasStyled;