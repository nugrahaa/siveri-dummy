import React from 'react';
import { shallow } from 'enzyme';
import { Dialog, DialogTitle } from '@material-ui/core';
import AddAdmin from './AddAdmin';

describe('<AddAdmin /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<AddAdmin />);
  });

  it('should render one <Dialog/>', () => {
    expect(wrapper.find(Dialog)).toHaveLength(1);
  });

  it('should render seven <DialogTitle />', () => {
    expect(wrapper.find(DialogTitle)).toHaveLength(1);
  });
});
