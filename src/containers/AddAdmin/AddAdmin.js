import React, { Component } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@material-ui/core';

export default class AddAdmin extends Component {
  render() {
    const { open, handleClose } = this.props;
    return (
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Tambah Admin</DialogTitle>
        <DialogContent>
          <TextField margin="dense" label="Akun SSO" fullWidth />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button color="primary">Submit</Button>
        </DialogActions>
      </Dialog>
    );
  }
}
