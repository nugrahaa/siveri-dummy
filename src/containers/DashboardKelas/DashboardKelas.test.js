import React from 'react';
import { shallow } from 'enzyme';
import DashboardKelas from './DashboardKelas';
import { Button, Grid } from '@material-ui/core';
import NavbarUser from '../../components/NavbarUser/NavbarUser';

describe('<DashboardKelas /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<DashboardKelas />);
  });

  it('should render one <NavbarUser />', () => {
    expect(wrapper.find(NavbarUser)).toHaveLength(1);
  });

  it('should render five <Grid />', () => {
    expect(wrapper.find(Grid)).toHaveLength(5);
  });

  it('should render two <Button />', () => {
    expect(wrapper.find(Button)).toHaveLength(2);
  });

});
