import styled from 'styled-components';

const DashboardKelasStyled = styled.div`
    .main-container {
        margin-top: 50px;
    }    
    .img-header {
        width: 100%;
    }
    .button {
        width: 200px;
        background-color: #2E313C;
        color: white;
        border-radius: 10px;
        margin-top: 10px;
    }
    .content-container {
        margin-top: 30px;
    }
`;

export default DashboardKelasStyled;