import {
    Grid,
    Button
} from "@material-ui/core";
import React, { Component } from "react";
import NavbarUser from "../../components/NavbarUser/NavbarUser";
import DashboardKelasStyled from "./style";
import HeaderKelas from "../../assets/kelas-header.jpg";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";

class DashboardKelas extends Component {
    render() {
        return (
            <DashboardKelasStyled>
                <NavbarUser />
                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                    className="main-container"
                >
                    <Grid md={10} item className="header-container">
                        <img
                            src={HeaderKelas}
                            alt="Header Kelas"
                            className="img-header"
                        />
                    </Grid>
                    <Grid md={10} item container className="content-container">
                        <Grid md={3} item>
                            <Button
                                variant="contained"
                                color="default"
                                className="button"
                                startIcon={<CloudUploadIcon />}
                            >
                                Tambah Topik
                            </Button>
                            <Button
                                variant="contained"
                                color="default"
                                className="button"
                                startIcon={<CloudUploadIcon />}
                            >
                                Upload
                            </Button>
                        </Grid>
                        <Grid md={9} item container>
                            
                        </Grid>
                    </Grid>
                </Grid>
            </DashboardKelasStyled>
        );
    }
}

export default DashboardKelas;
