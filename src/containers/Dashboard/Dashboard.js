import React, { Component } from 'react';
import {
  Typography,
  TableContainer,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
} from '@material-ui/core';
import Navbar from '../../components/Navbar/Navbar';

export default class Dashboard extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <Typography variant="h3" gutterBottom>
          Selamat datang, Admin!
        </Typography>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>No.</TableCell>
                <TableCell align="right">Nama</TableCell>
                <TableCell align="right">Akun SSO</TableCell>
                <TableCell align="right">Email</TableCell>
                <TableCell align="right">Peran</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>1.</TableCell>
                <TableCell align="right">John Doe</TableCell>
                <TableCell align="right">john.doe</TableCell>
                <TableCell align="right">john.doe@ui.ac.id</TableCell>
                <TableCell align="right">Admin</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}
