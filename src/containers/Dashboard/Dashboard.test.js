import React from 'react';
import { shallow } from 'enzyme';
import { TableContainer } from '@material-ui/core';
import Dashboard from './Dashboard';
import Navbar from '../../components/Navbar/Navbar';

describe('<Dashboard /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Dashboard />);
  });

  it('should render one <Navbar />', () => {
    expect(wrapper.find(Navbar)).toHaveLength(1);
  });

  it('should render seven <TableContainer />', () => {
    expect(wrapper.find(TableContainer)).toHaveLength(1);
  });
});
