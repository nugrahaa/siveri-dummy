import { Grid } from "@material-ui/core";
import { shallow } from "enzyme";
import { Helmet } from "react-helmet";
import CreateKelas from "./CreateKelas";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";

describe("<DashboardUser /> testing", () => {
    const initialState = {};    
    const mockStore = configureStore();
    let store;
    let wrapper;

    beforeEach(() => {
        store = mockStore(initialState);

        wrapper = shallow(
            <Provider store={store}>
                <CreateKelas />
            </Provider>
        ).dive();

        console.log(wrapper.debug());
    });

    it("should render one <Helmet /> elements", () => {
        expect(wrapper.find(Helmet)).toHaveLength(0);
    });

    it("should render two <Grid /> elements", () => {
        expect(wrapper.find(Grid)).toHaveLength(0);
    });
});
