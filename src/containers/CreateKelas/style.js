import styled from "styled-components";

const CreateKelasStyled = styled.div`
  .title {
    margin-bottom: 10px;
  }
  .main-container {
    margin: 30px 50px;
  }
  .card-container {
    margin-top: 30px;
  }
  .info {
    font-size: 30px;
  }

  .form-container{
      margin-top:40px;
  }


  .create-button {
    margin-top:60px;
    background-color: #2E313C;
    color: white;
    font-weight: bold;
    font-size: 15px;
    width: 200px;
    height: 40px;
    border-radius: 10px;
  }

  .className-form{
      width:500px;
  }
`;

export default CreateKelasStyled;
