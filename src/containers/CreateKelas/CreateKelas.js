import { Divider, Grid, Typography } from "@material-ui/core";
import React, { Component } from "react";
import NavbarUser from "../../components/NavbarUser/NavbarUser";
import CreateKelasStyled from "./style";
import { Helmet } from "react-helmet";
import favicon from "../Dashboard-user/assets/ui.png";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { fetchProfile } from "../../store/actions/profile";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import Button from "@material-ui/core/Button";

class CreateKelas extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.props.fetchProfile();
  }

  render() {
    if (this.props.isLoading) {
      const data = this.props.user[0];
      return (
        <CreateKelasStyled>
          <Helmet>
            <title>FEB UI</title>
            <link rel="icon" type="image/png" href={favicon} />
          </Helmet>
          <NavbarUser
            clicked={() => this.handleLogout()}
            name={data.full_name}
            role={data.profile.role}
            npm={data.profile.npm}
          />
          <Grid container direction="column" className="main-container">
            <Grid item md={6}>
              <Typography className="title" variant="h4">
                Buat Kelas
              </Typography>
              <Divider />

			  <Grid container className="main-container" direction="row"> 
				<Grid item md={12} className="form-container">
					<FormControl variant="outlined">
						<InputLabel htmlFor="component-outlined">
							Nama Kelas
						</InputLabel>
						<OutlinedInput id="component-outlined" label="Nama Kelas" className="className-form"/>
					</FormControl>
				</Grid >
				<Grid
				container
				direction="row"
				justify="left"
				alignItems="center"
				className="form-container"
				>
					<Grid item md={3} >
						<FormControl variant="outlined">
							<InputLabel htmlFor="component-outlined">
							Enrollment Key
							</InputLabel>
							<OutlinedInput id="component-outlined" label="Nama Kelas" />
						</FormControl>
					</Grid>
					
					<Grid item md={9}>
						<Button>Generate</Button>
					</Grid>
		  		</Grid>
				<Grid item md={12} >
						<Button variant="contained" color="primary" className="create-button" >Buat</Button>
				</Grid>
				  </Grid>
				</Grid>
			</Grid>
        </CreateKelasStyled>
      );
    }
    return (
      <CreateKelasStyled>
        <Helmet>
          <title>FEB UI</title>
          <link rel="icon" type="image/png" href={favicon} />
        </Helmet>
      </CreateKelasStyled>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.dashBoardUserReducer.user,
    isLoading: state.dashBoardUserReducer.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    push: (url) => dispatch(push(url)),
    fetchProfile: () => dispatch(fetchProfile()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateKelas);
