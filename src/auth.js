import axios from 'axios';

const auth = {

    login(user) {
        if (auth.loggedIn()) {
            return Promise.resolve(true);
        }
        console.log(user);
        localStorage.mpkt2021 = JSON.stringify(user);

        return Promise.resolve(true);
    },

    logout() {
        localStorage.removeItem("mpkt2021");
        Reflect.deleteProperty(axios.defaults.headers.common, "Authorization");
    },

    // Check if the user logged in or not
    loggedIn() {
        const notUser = Boolean(localStorage.mpkt2021)

        console.log("MASUK KE LOGGED INNNNN")

        return notUser ? JSON.parse(localStorage.mpkt2021) : false
    }
}

export default auth