import { Grid, Typography } from '@material-ui/core';
import { shallow } from 'enzyme';
import ProfileCard from './ProfileCard';

describe("<ServiceCard /> testing", () => {
    let wrapper;

    beforeEach(() => {
      wrapper = shallow(<ProfileCard />);
    });
  
    it('should render three <Grid /> elements', () => {
      expect(wrapper.find(Grid)).toHaveLength(3);
    });
  
    it('should render one <Typograpgy /> elements', () => {
      expect(wrapper.find(Typography)).toHaveLength(7);
    });
  
});
