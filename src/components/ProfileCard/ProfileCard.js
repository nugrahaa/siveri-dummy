import { Grid, Typography, Avatar } from "@material-ui/core";
import React from "react";

import ProfileCardStyle from "./style.js";

export default function ProfileCard(props) {
    return (
        <ProfileCardStyle>
            <Grid
                container
                direction="column"
                alignItems="center"
                className="profil-card"
            >
                <Grid item>
                    <Avatar className="avatar-pic">J</Avatar>
                </Grid>
                <Grid item xs>
                    <Typography className="profile-name" align="center">
                        <Typography className="name" variant="body1">
                            {props.name}
                        </Typography>
                        <Typography className="role" variant="body1">
                            {props.role}
                        </Typography>
                        <Typography className="npm" variant="body1">
                            {props.npm}
                        </Typography>
                    </Typography>
                    <Typography className="faculty-name" align="center">
                        <Typography className="major" variant="body1">
                            {props.major}
                        </Typography>
                        <Typography className="faculty" variant="body1">
                            Fakultas {props.faculty}
                        </Typography>
                    </Typography>
                </Grid>
            </Grid>
        </ProfileCardStyle>
    );
}
