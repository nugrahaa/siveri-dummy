import styled from "styled-components";

const ProfileCardStyle = styled.div`
    .profil-card{
        background-color: #D5CEC5;
        box-shadow: -4px 4px 4px rgba(0, 0, 0, 0.1);
        border-radius: 20px;
        text-align: right;
        padding-top:50px;
        padding-bottom:50px;
        text-justify : center;
        margin-top:40px;
        margin-left:10px;
    }
    
    .avatar-pic{
        height:150px;
        width:150px;
    }

    .name{
        font-size:28px;
        font-weight:bold;
        margin-top: 20px;
    }

    .role{
        text-transform: capitalize;
        font-size:20px;
    }

    .npm{
        font-size:22px;
        padding-bottom:30px;
    }

    .faculty-name p{
        padding:0px;
        margin:0px;
        padding: 0px 20px;
    }

    .major{
        font-size:25px;
    }

    .faculty{
        font-size:30px;
        font-weight:bold;
    }
`;

export default ProfileCardStyle;
