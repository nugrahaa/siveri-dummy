import React from 'react';
import { shallow } from 'enzyme';
import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid, Typography } from '@material-ui/core';
import CardKelas from './CardKelas';

describe('<CardKelas /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<CardKelas />);
  });

  it('should render one <Card />', () => {
    expect(wrapper.find(Card)).toHaveLength(1);
  });

  it('should render one <CardActionArea />', () => {
    expect(wrapper.find(CardActionArea)).toHaveLength(1);
  });

  it('should render one <CardMedia />', () => {
    expect(wrapper.find(CardMedia)).toHaveLength(1);
  });

  it('should render one <CardContent />', () => {
    expect(wrapper.find(CardContent)).toHaveLength(1);
  });

  it('should render two <Typography />', () => {
    expect(wrapper.find(Typography)).toHaveLength(2);
  });

  it('should render two <CardActions />', () => {
    expect(wrapper.find(CardActions)).toHaveLength(1);
  });

  it('should render two <Button />', () => {
    expect(wrapper.find(Button)).toHaveLength(1);
  });

  it('should render no <Grid />', () => {
    expect(wrapper.find(Grid)).toHaveLength(0);
  });

});
