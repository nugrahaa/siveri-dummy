import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import FlatBackground from "../../assets/flat-1.jpg";

class CardKelas extends Component {
    render() {
        return (
            <Card
                style={{ margin: "10px", marginTop: "50px", maxWidth: 345 }}
            >
                <CardActionArea>
                    <CardMedia
                        style={{height: 140}}
                        image={FlatBackground}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {this.props.title}
                        </Typography>
                        <Typography
                            variant="body2"
                            color="textSecondary"
                            component="p"
                        >
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Integer nec cursus magna. Pellentesque dolor
                            elit, pretium eu ligula vitae, venenatis rhoncus
                            lorem.
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size="small" color="primary">
                        Masuk
                    </Button>
                </CardActions>
            </Card>
        );
    }
}

export default CardKelas;
