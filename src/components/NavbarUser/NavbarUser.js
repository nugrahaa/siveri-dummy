import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Drawer from "@material-ui/core/Drawer";
import LogoFeb from "../../assets/logo-feb-hor.png";
import { Divider, Grid, Avatar } from "@material-ui/core";
import NavbarUserStyled from "./style";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ClassRoundedIcon from "@material-ui/icons/ClassRounded";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import WorkRoundedIcon from "@material-ui/icons/WorkRounded";
import DashboardRoundedIcon from "@material-ui/icons/DashboardRounded";
import Button from '@material-ui/core/Button';

class NavbarUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
    }

    handleDrawer = (status) => {
        this.setState({
            open: status,
        });
    };

    render(props) {
        return (
            <NavbarUserStyled>
                <AppBar
                    position="static"
                    elevation={1}
                    style={{ background: "#ffff" }}
                >
                    <Toolbar>
                        <div style={{ flexGrow: 1 }}>
                            <img
                                src={LogoFeb}
                                style={{
                                    width: "150px",
                                    padding: "10px 0",
                                    marginLeft: "20px",
                                }}
                                alt="Logo FEB"
                            />
                        </div>
                        <div>
                            <IconButton
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={() => this.handleDrawer(true)}
                                color="inherit"
                            >
                                <MenuIcon style={{ fill: "black" }} />
                            </IconButton>
                        </div>
                    </Toolbar>
                </AppBar>

                <Drawer
                    anchor="right"
                    open={this.state.open}
                    onClose={() => this.handleDrawer(false)}
                >
                    <Grid
                        container
                        direction="row"
                        style={{ marginRight: "80px" }}
                    >
                        <Grid item>
                        <Avatar 
                                style={{ height: "80px", width: "80px",margin : "10px"}}
                                alt="Foto Profil">J</Avatar>
                        </Grid>
                        <Grid item>
                            <Typography
                                variant="h6"
                                style={{
                                    fontWeight: "bold",
                                    fontSize: "20px",
                                    marginTop: "20px",
                                }}
                            >
                                {this.props.name}
                            </Typography>
                            <Typography variant="body1">{this.props.role}</Typography>
                            <Typography variant="body1">{this.props.npm}</Typography>
                        </Grid>
                    </Grid>
                    <Divider />
                    <List
                        component="nav"
                        aria-labelledby="nested-list-subheader"
                        subheader={
                            <ListSubheader
                                component="div"
                                id="nested-list-subheader"
                            >
                                Menu
                            </ListSubheader>
                        }
                    >
                        <ListItem button>
                            <ListItemIcon>
                                <DashboardRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Dashboard" />
                        </ListItem>
                        <ListItem button>
                            <ListItemIcon>
                                <ClassRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="MPKT 5 SKS" />
                        </ListItem>
                        <ListItem button>
                            <ListItemIcon>
                                <WorkRoundedIcon />
                            </ListItemIcon>
                            <ListItemText primary="Layanan Magang" />
                        </ListItem>
                        <Button variant="contained" color="secondary" style={{width: "90%", margin: "10px"}} onClick={this.props.clicked}>
                            Logout
                        </Button>
                    </List>
                </Drawer>
            </NavbarUserStyled>
        );
    }
}

export default NavbarUser;
