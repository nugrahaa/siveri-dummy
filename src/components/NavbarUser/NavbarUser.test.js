import React from 'react';
import { shallow } from 'enzyme';
import { IconButton, AppBar, Toolbar, Drawer, Grid, Typography, Divider, List, ListItem, ListItemIcon, Button, CardMedia } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NavbarUser from './NavbarUser';

describe('<NavbarUser /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<NavbarUser />);
  });

  it('should render one <Appbar />', () => {
    expect(wrapper.find(AppBar)).toHaveLength(1);
  });

  it('should render one <Toolbar />', () => {
    expect(wrapper.find(Toolbar)).toHaveLength(1);
  });

  it('should render one <IconButton />', () => {
    expect(wrapper.find(IconButton)).toHaveLength(1);
  });

  it('should render one <MenuIcon />', () => {
    expect(wrapper.find(MenuIcon)).toHaveLength(1);
  });

  it('should render one <Drawer />', () => {
    expect(wrapper.find(Drawer)).toHaveLength(1);
  });

  it('should render three <Grid />', () => {
    expect(wrapper.find(Grid)).toHaveLength(3);
  });

  it('should render three <Typography />', () => {
    expect(wrapper.find(Typography)).toHaveLength(3);
  });

  it('should render one <Divider />', () => {
    expect(wrapper.find(Divider)).toHaveLength(1);
  });

  it('should render one <List />', () => {
    expect(wrapper.find(List)).toHaveLength(1);
  });

  it('should render three <ListItem />', () => {
    expect(wrapper.find(ListItem)).toHaveLength(3);
  });

  it('should render one <ListItemIcon />', () => {
    expect(wrapper.find(ListItemIcon)).toHaveLength(3);
  });
  
  it('should render one <Button />', () => {
    expect(wrapper.find(Button)).toHaveLength(1);
  });

  it('should not render <CardMedia />', () => {
    expect(wrapper.find(CardMedia)).toHaveLength(0);
  });

});
