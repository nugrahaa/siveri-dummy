import styled from "styled-components";

const NavbarUserStyled = styled.div`
    .menubutton {
        margin-right: theme.spacing(2),
    }
    .title {
        flex-grow: 1,
    }
`;

export default NavbarUserStyled;
