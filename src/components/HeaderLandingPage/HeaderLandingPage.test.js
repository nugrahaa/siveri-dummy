import { Grid, Typography, CardMedia } from "@material-ui/core";
import { shallow } from "enzyme";
import HeaderLandingPage from "./HeaderLandingPage";

describe('"<HeaderLandingPage /> testing', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<HeaderLandingPage />);
    });

    it("should render three <Grid /> elements", () => {
        expect(wrapper.find(Grid)).toHaveLength(3);
    });

    it("should render one <Typography /> elements", () => {
        expect(wrapper.find(Typography)).toHaveLength(1);
    });

    it("should not render <CardMedia /> elements", () => {
        expect(wrapper.find(CardMedia)).toHaveLength(0);
    });

});
