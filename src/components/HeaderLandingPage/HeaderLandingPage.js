import { Grid, Typography } from "@material-ui/core";
import React from "react";
import logo from "../../assets/feb-logo.png";
import Base from "./style";

export default function HeaderLandingPage() {
    return (
        <Base>
            <Grid 
                container 
                alignItems="center" 
                justify="center">
                <Grid item xs={2} className="logo">
                    <img src={logo} className="logo" alt="Logo FEB"/>
                </Grid>
                <Grid item xs={7}>
                    <Typography className="title" variant="h6">
                        Layanan Kegiatan MPKT 5 SKS dan Magang Fakultas Ekonomi
                        dan Bisnis
                    </Typography>
                </Grid>
            </Grid>
        </Base>
    );
}
