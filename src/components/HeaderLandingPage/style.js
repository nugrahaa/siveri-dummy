import styled from 'styled-components';

const Base = styled.div`
    .logo {
        width: 80px
    }
    .title {
        font-weight: bold;
        color: #2c3e50;
    }
`;

export default Base;
