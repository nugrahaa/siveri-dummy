import { Grid } from '@material-ui/core';
import { shallow } from 'enzyme';
import LoginButton from './LoginButton';
import Base from './style';

describe("<LoginButton /> testing", () => {

  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<LoginButton />);
  });

  it('should render one <Base /> elements', () => {
    expect(wrapper.find(Base)).toHaveLength(1);
  });

  it('should not render <Grid /> elements', () => {
    expect(wrapper.find(Grid)).toHaveLength(0);
  });

});


