import React from 'react';
import Base from './style';

export default function LoginButton(props) {
    return (
        <Base className="my-button" onClick={props.clicked}>
            {props.children}
        </Base>
    )
}
