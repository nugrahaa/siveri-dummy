import styled from "styled-components";

const Base = styled.button`
    background-color: #626375;
    color: white;
    font-weight: bold;
    font-size: 15px;
    width: 300px;
    height: 40px;
    border: none;
    border-radius: 12px;
`;

export default Base;
