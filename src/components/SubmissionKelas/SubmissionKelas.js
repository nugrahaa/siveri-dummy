import { Button, Divider, Typography } from "@material-ui/core";
import React, { Component } from "react";
import SubmissionKelasStyled from "./style";
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';

class SubmissionKelas extends Component {
    render() {
        return (
            <SubmissionKelasStyled>
                <Typography variant="h5" className="title">Topik: Mengajar</Typography>
                <Divider />
                <Button variant="contained" className="button" startIcon={<AssignmentIndIcon />}>Submission Laporan</Button>
            </SubmissionKelasStyled>
        );
    }
}

export default SubmissionKelas;
