import styled from 'styled-components';

const SubmissionKelasStyled = styled.div`
    width: 100%;
    .title {
        margin-bottom: 5px;
    }
    .button {
        background-color: #E0E0E0;
        color: #2E313C;
        width: 100%;
        height: 50px;
        font-size: 15px;
        text-align: start;
    }
`;

export default SubmissionKelasStyled;