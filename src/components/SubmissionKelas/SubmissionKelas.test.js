import React from 'react';
import { shallow } from 'enzyme';
import SubmissionKelas from './SubmissionKelas';
import { Button, CardMedia, Divider, Typography } from '@material-ui/core';

describe('<SubmissionKelas /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<SubmissionKelas />);
  });

  it('should render one <Typography />', () => {
    expect(wrapper.find(Typography)).toHaveLength(1);
  });

  it('should render one <Divider />', () => {
    expect(wrapper.find(Divider)).toHaveLength(1);
  });

  it('should render one <Button />', () => {
    expect(wrapper.find(Button)).toHaveLength(1);
  });

  it('should not render <CardMedia />', () => {
    expect(wrapper.find(CardMedia)).toHaveLength(0);
  });

});
