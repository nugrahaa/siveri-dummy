import { Grid, Typography } from '@material-ui/core';
import ListItem from "@material-ui/core/ListItem";
import { shallow } from 'enzyme';
import ServiceCard from './ServiceCard';

describe("<ServiceCard /> testing", () => {
    let wrapper;

    beforeEach(() => {
      wrapper = shallow(<ServiceCard />);
    });
  
    it('should render one <ListItem /> elements', () => {
      expect(wrapper.find(ListItem)).toHaveLength(1);
    });
  
    it('should render one <Typography /> elements', () => {
      expect(wrapper.find(Typography)).toHaveLength(1);
    });

    it('should not render <Grid /> elements', () => {
      expect(wrapper.find(Grid)).toHaveLength(0);
    });
  
  
});
