import { Typography } from "@material-ui/core";
import React from 'react';
import ServiceCardStyle from './style.js';
import ListItem from "@material-ui/core/ListItem";

export default function ServiceCard(props) {
    return (
        <ServiceCardStyle>
            <ListItem button
                container
                className="title-card">
                    <Typography variant = "button" className ="title">
                        {props.type}
                    </Typography>
            </ListItem>
        </ServiceCardStyle>
    )
}
