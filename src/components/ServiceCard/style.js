import styled from "styled-components";

const ServiceCardStyle = styled.div`
    .title-card{
        background-color: #C4C4C4;
        box-shadow: -4px 4px 4px rgba(0, 0, 0, 0.1);
        border-radius: 20px;
        padding: 20px 0;
        margin-right : 20px;
        width:650px;
        margin-top: 40px;
    }
    .title{
        font-weight: bold;
        font-size :36px;
        margin-left:40px;
    }
`;

export default ServiceCardStyle;
