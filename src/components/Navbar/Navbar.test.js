import React from 'react';
import { shallow } from 'enzyme';
import { IconButton, Drawer } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import Navbar from './Navbar';

describe('<Navbar /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Navbar />);
  });

  it('should render one <MenuIcon />', () => {
    expect(wrapper.find(MenuIcon)).toHaveLength(1);
  });

  it('should render one <IconButton />', () => {
    expect(wrapper.find(IconButton)).toHaveLength(1);
  });

  it('should has state isOpenSideBar to be true when first <IconButton/> is clicked', () => {
    const button = wrapper.find(IconButton).first();
    button.simulate('click');
    expect(wrapper.state('isOpenSideBar')).toEqual(true);
  });

  it('should has state isOpenSideBar to be false by default', () => {
    expect(wrapper.state('isOpenSideBar')).toEqual(false);
  });

  it('should has default state when first <IconButton/> is clicked and close again', () => {
    const button = wrapper.find(IconButton).first();
    button.simulate('click');
    wrapper.find(Drawer).simulate('close');
    expect(wrapper.state('isOpenSideBar')).toEqual(false);
  });
});
