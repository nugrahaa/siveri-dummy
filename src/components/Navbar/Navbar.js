import React from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Drawer,
  Box,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import AddAdmin from '../../containers/AddAdmin/AddAdmin';
import NavList from '../NavList/NavList';

const menuMagang = {
  menuName: 'Pengaturan Magang',
  menuIcon: 'people',
  menuList: [
    {
      href: '',
      id: 1,
      title: 'Daftar Mahasiswa Magang',
    },
    {
      href: '',
      id: 2,
      title: 'Daftar Kegiatan Magang',
    },
  ],
};

const menuAdmin = {
  menuName: 'Pengaturan Admin',
  menuIcon: 'settings',
  menuList: [
    {
      href: '',
      id: 3,
      title: 'Tambah Admin',
    },
  ],
};

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenSideBar: false,
      isOpenAddAdmin: false,
    };
    this.handleToggleSideBar = this.handleToggleSideBar.bind(this);
    this.handleToggleAddAdmin = this.handleToggleAddAdmin.bind(this);
  }

  handleToggleSideBar() {
    this.setState((state) => ({
      isOpenSideBar: !state.isOpenSideBar,
    }));
  }

  handleToggleAddAdmin() {
    this.setState((state) => ({
      isOpenAddAdmin: !state.isOpenAddAdmin,
    }));
  }

  render() {
    const { isOpenSideBar, isOpenAddAdmin } = this.state;
    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <Box display="flex" flexGrow={1}>
              <Typography variant="h6">Dashboard</Typography>
            </Box>
            <IconButton
              edge="end"
              color="inherit"
              aria-label="menu"
              onClick={this.handleToggleSideBar}
            >
              <MenuIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer
          open={isOpenSideBar}
          anchor="right"
          role="presentation"
          onClose={this.handleToggleSideBar}
        >
          <NavList menu={menuMagang} />
          <NavList menu={menuAdmin} click={this.handleToggleAddAdmin} />
        </Drawer>
        <AddAdmin
          open={isOpenAddAdmin}
          handleClose={this.handleToggleAddAdmin}
        />
      </div>
    );
  }
}

export default Navbar;
