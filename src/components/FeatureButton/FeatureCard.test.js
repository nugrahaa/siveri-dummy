import React from 'react';
import { Grid, Typography, CardMedia } from '@material-ui/core';
import { shallow } from 'enzyme';
import FeatureCard from './FeatureCard';

describe('<FeatureCard /> testing', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<FeatureCard />);
  });

  it('should render three <Grid /> elements', () => {
    expect(wrapper.find(Grid)).toHaveLength(3);
  });

  it('should render two <Typograpgy /> elements', () => {
    expect(wrapper.find(Typography)).toHaveLength(2);
  });

  it('should not render <CardMedia /> elements', () => {
    expect(wrapper.find(CardMedia)).toHaveLength(0);
  });

});
