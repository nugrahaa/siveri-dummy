import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import FeatureCardStyle from './style';

export default function FeatureCard(props) {
  const { type, detail } = props;
  return (
    <FeatureCardStyle>
      <Grid container alignItems="center" justify="center" direction="row">
        <Grid item md={3} className="title-card">
          <Typography variant="button" className="title">
            {type}
          </Typography>
        </Grid>
        <Grid item md={7}>
          <Typography className="about">{detail}</Typography>
        </Grid>
      </Grid>
    </FeatureCardStyle>
  );
}
