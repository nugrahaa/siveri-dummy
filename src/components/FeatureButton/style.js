import styled from 'styled-components';

const FeatureCardStyle = styled.div`
  .title {
    font-weight: bold;
  }
  .title-card {
    background: #f5f5f8;
    box-shadow: -4px 4px 4px rgba(0, 0, 0, 0.1);
    border-radius: 10px;
    text-align: center;
    padding: 30px 0;
    margin-right: 10px;
  }
  .about {
    font-weight: 300;
    font-size: 14px;
  }
`;

export default FeatureCardStyle;
