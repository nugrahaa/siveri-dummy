import React from 'react';
import { shallow } from 'enzyme';
import { Accordion, AccordionDetails } from '@material-ui/core';
import NavList from './NavList';

describe('<NavList /> testing', () => {
  let wrapper;
  const menuDummy = {
    menuName: 'Dummy',
    menuIcon: 'people',
    menuList: [
      {
        href: '',
        title: 'Dummy 1',
      },
      {
        href: '',
        title: 'Dummy 2',
      },
    ],
  };

  beforeEach(() => {
    wrapper = shallow(<NavList menu={menuDummy} />);
  });

  it('should render one <Accordion />', () => {
    expect(wrapper.find(Accordion)).toHaveLength(1);
  });

  it('should render one <AccordionDetails />', () => {
    expect(wrapper.find(AccordionDetails)).toHaveLength(1);
  });
});
