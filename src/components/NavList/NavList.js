import React from 'react';
import {
  Typography,
  IconButton,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PanoramaFishEyeIcon from '@material-ui/icons/PanoramaFishEye';
import PeopleIcon from '@material-ui/icons/People';
import SettingsIcon from '@material-ui/icons/Settings';

const NavList = (props) => {
  const { menuName, menuIcon, menuList } = props.menu;
  const { click } = props;
  return (
    <div>
      <Accordion>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <IconButton edge="start" color="inherit">
            {menuIcon === 'people' ? <PeopleIcon /> : <SettingsIcon />}
          </IconButton>
          <Typography>{menuName}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <List>
            {menuList.map((menu) => (
              <ListItem button onClick={click} key={menu.id}>
                <ListItemIcon>
                  <PanoramaFishEyeIcon />
                </ListItemIcon>
                <ListItemText primary={menu.title} />
              </ListItem>
            ))}
          </List>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default NavList;
