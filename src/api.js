import auth from "./auth";
import axios from "axios";

let MAIN_URL = "https://siveri-dev.herokuapp.com";
// let MAIN_URL = "http://127.0.0.1:8000";


let API_PREFIX_URL_AUTH = `${MAIN_URL}/sso`;
let API_PREFIX_URL = `${MAIN_URL}`;

axios.interceptors.request.use(
    (config) => {
        const user = auth.loggedIn();

        if (user) {
            const token = user;
            if (token) {
                config.headers.Authorization = `Bearer ${token}`;
            }
        }

        return config;
    },
    (error) => Promise.reject(error)
);

export const loggingInApi = `${API_PREFIX_URL_AUTH}/login`;
export const loggingOutApi = `${API_PREFIX_URL_AUTH}/logout`;
export const fetchProfileApi = `${API_PREFIX_URL_AUTH}/profile/`;
export const fetchListKelasApi = `${API_PREFIX_URL}/kelas/`;
