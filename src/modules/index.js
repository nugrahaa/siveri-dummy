import { connectRouter } from "connected-react-router";
import { combineReducers } from "redux";
import globalReducer from "../store/reducers/globalReducer";
import dashBoardUserReducer from "../store/reducers/profile";
import listKelasReducer from '../store/reducers/listKelas';

export default (history) =>
  combineReducers({
    router: connectRouter(history),
    globalReducer: globalReducer,
    dashBoardUserReducer : dashBoardUserReducer,
    listKelasReducer: listKelasReducer
});
